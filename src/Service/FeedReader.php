<?php

namespace App\Service;

use FeedIo\FeedInterface;
use FeedIo\FeedIo;

class FeedReader
{
    protected $feed;
    protected $url;
    protected $frequencyCounter;
    protected $commonWords;

    protected $feedCache;

    public function __construct(FeedIo $feed, string $url, WordFrequencyCounter $frequencyCounter, array $commonWords)
    {
        $this->feed = $feed;
        $this->url = $url;
        $this->frequencyCounter = $frequencyCounter;
        $this->commonWords = $commonWords;
    }

    public function getFeed(): FeedInterface
    {
        if (!$this->feedCache) {
            $this->feedCache = $this->feed->read($this->url)->getFeed();
        }

        return $this->feedCache;
    }

    public function getFrequentWords(int $count): array
    {
        $stats = $this->frequencyCounter->getWordStatistics($this->getFeed());
        $words = $this->filterCommonWords($stats);
        arsort($words);

        return array_slice($words, 0, $count, true);
    }

    protected function filterCommonWords(array $words): array
    {
        foreach ($this->commonWords as $word) {
            if (key_exists($word, $words)) {
                unset($words[$word]);
            }
        }

        return $words;
    }
}
