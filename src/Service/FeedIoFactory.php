<?php

namespace App\Service;

use Csa\GuzzleHttp\Middleware\Cache\Adapter\PsrAdapter;
use Csa\GuzzleHttp\Middleware\Cache\CacheMiddleware;
use DateInterval;
use FeedIo\Adapter\Guzzle\Client as HttpAdapter;
use FeedIo\FeedIo;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Throwable;

class FeedIoFactory
{
    use LoggerAwareTrait;

    protected $cache;
    protected $cacheTTL;
    protected $debug;

    public function __construct(LoggerInterface $logger, CacheItemPoolInterface $cache, string $cacheTTL, bool $debug)
    {
        $this->setLogger($logger);
        $this->cache = $cache;
        $this->cacheTTL = $cacheTTL;
        $this->debug = $debug;
    }

    public function create(): FeedIo
    {
        return new FeedIo(new HttpAdapter($this->getHttpClient()), $this->logger);
    }

    protected function getHttpClient(): ClientInterface
    {
        $cacheAdapter = new PsrAdapter($this->cache, $this->getCacheTTL());
        $cacheMiddleware = new CacheMiddleware($cacheAdapter, $this->debug);

        $handler = HandlerStack::create();
        $handler->push($cacheMiddleware, 'cache');

        return new HttpClient(['handler' => $handler]);
    }

    protected function getCacheTTL(): DateInterval
    {
        try {
            return new DateInterval($this->cacheTTL);
        } catch (Throwable $e) {
            throw new RuntimeException('Invalid value for cache TTL: ' . $this->cacheTTL, $e->getCode(), $e);
        }
    }
}
