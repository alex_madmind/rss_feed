<?php

namespace App\Service;

use FeedIo\Feed\Item;
use FeedIo\FeedInterface;
use Psr\Cache\CacheItemPoolInterface;

class WordFrequencyCounter
{
    protected $cachePool;

    public function __construct(CacheItemPoolInterface $cachePool)
    {
        $this->cachePool = $cachePool;
    }

    public function getWordStatistics(FeedInterface $feed): array
    {
        $result = [];
        $cid = 'word_stats_' . md5($feed->getLink() . $feed->getPublicId());

        $cache = $this->cachePool->getItem($cid);
        if ($cache->isHit()) {
            return $cache->get();
        }

        foreach ($feed as $item) {
            $words = $this->getWords($item);

            foreach ($words as $word) {
                if (!isset($result[$word])) {
                    $result[$word] = 1;
                } else {
                    $result[$word]++;
                }
            }
        }

        $cache->set($result);
        $this->cachePool->save($cache);

        return $result;
    }

    protected function getWords(Item $item): array
    {
        $title = $item->getTitle();
        $description = strip_tags($item->getDescription());

        $text = mb_strtolower($title . ' ' . $description);
        $text = str_replace(['.', ',', '!', '?', ':', '…', '–', '(', ')', '"'], ' ', $text);

        $words = array_map('trim', explode(' ', $text));
        $words = array_filter(array_unique($words));
        $words = array_filter($words, function ($word) {
            return !is_numeric($word);
        });

        return array_values($words);
    }
}
