<?php

namespace App\Controller;

use App\Service\FeedReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FeedController extends AbstractController
{
    /**
     * @Route("/", name="feed")
     */
    public function index(FeedReader $reader)
    {
        return $this->render('feed/index.html.twig', [
            'words' => $reader->getFrequentWords(10),
            'feed' => $reader->getFeed(),
        ]);
    }
}
