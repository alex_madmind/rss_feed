<?php

namespace App\Tests\Unit\Service;

use App\Service\FeedIoFactory;
use DateInterval;
use GuzzleHttp\ClientInterface;

class FeedIoFactoryStub extends FeedIoFactory
{
    public function getCacheTTL(): DateInterval
    {
        return parent::getCacheTTL();
    }

    public function getHttpClient(): ClientInterface
    {
        return parent::getHttpClient();
    }
}
