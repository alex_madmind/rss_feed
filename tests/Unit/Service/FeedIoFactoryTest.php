<?php

namespace App\Tests\Unit\Service;

use App\Tests\Unit\TestCase;
use Csa\GuzzleHttp\Middleware\Cache\CacheMiddleware;
use DateInterval;
use GuzzleHttp\ClientInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;

class FeedIoFactoryTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|LoggerInterface
     */
    protected $logger;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|CacheItemPoolInterface
     */
    protected $cache;

    protected function setUp()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->cache = $this->createMock(CacheItemPoolInterface::class);
    }

    public function testGetDuration()
    {
        $service = $this->createService();

        $result = $service->getCacheTTL();

        $this->assertEquals(new DateInterval('PT5M'), $result);
    }

    public function testGetDurationException()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Invalid value for cache TTL: test');

        $service = $this->createService('test');
        $service->getCacheTTL();
    }

    public function testGetHttpClient()
    {
        $service = $this->createServiceMock(['getCacheTTL']);
        $service->expects($this->once())
            ->method('getCacheTTL')
            ->willReturn($this->createMock(DateInterval::class));

        $result = $service->getHttpClient();

        /* @var $handler \GuzzleHttp\HandlerStack */
        $handler = $result->getConfig('handler');
        $stack = $this->getObjectPropertyValue($handler, 'stack');
        $result = array_filter($stack, function ($value) {
            return $value[0] instanceof CacheMiddleware;
        });

        $this->assertCount(1, $result, 'HTTP client does not contain cache middleware');
    }

    public function testCreate()
    {
        $httpClientMock = $this->createMock(ClientInterface::class);
        $service = $this->createServiceMock(['getHttpClient']);
        $service->expects($this->once())
            ->method('getHttpClient')
            ->willReturn($httpClientMock);

        $result = $service->create();

        $adapter = $this->getObjectPropertyValue($result, 'client');
        $this->assertInstanceOf(\FeedIo\Adapter\Guzzle\Client::class, $adapter);

        $httpClient = $this->getObjectPropertyValue($adapter, 'guzzleClient');
        $this->assertSame($httpClientMock, $httpClient);
    }

    protected function createService(string $cacheTTL = 'PT5M', bool $debug = true)
    {
        return new FeedIoFactoryStub($this->logger, $this->cache, $cacheTTL, $debug);
    }

    /**
     * @param array $methods Methods to be mocked
     *
     * @return \PHPUnit\Framework\MockObject\MockObject|FeedIoFactoryStub
     */
    protected function createServiceMock(array $methods)
    {
        return $this->getMockBuilder(FeedIoFactoryStub::class)
            ->setConstructorArgs([$this->logger, $this->cache, 'PT5M', false])
            ->setMethods($methods)
            ->getMock();
    }
}
