<?php

namespace App\Tests\Unit\Service;

use App\Service\WordFrequencyCounter;
use FeedIo\Feed\Item;

class WordFrequencyCounterStub extends WordFrequencyCounter
{
    public function getWords(Item $item): array
    {
        return parent::getWords($item);
    }
}
