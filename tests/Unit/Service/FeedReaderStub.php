<?php

namespace App\Tests\Unit\Service;

use App\Service\FeedReader;

class FeedReaderStub extends FeedReader
{
    public function filterCommonWords(array $words): array
    {
        return parent::filterCommonWords($words);
    }
}
