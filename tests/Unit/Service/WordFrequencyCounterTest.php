<?php

namespace App\Tests\Unit\Service;

use App\Tests\Unit\TestCase;
use FeedIo\Feed\Item;
use Psr\Cache\CacheItemPoolInterface;

class WordFrequencyCounterTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|CacheItemPoolInterface
     */
    protected $cachePool;

    protected function setUp()
    {
        $this->cachePool = $this->createMock(CacheItemPoolInterface::class);
    }

    public function wordsData()
    {
        return [
            'punctuation' => [
                'Here, are. some! words:   with? – punctuation…',
                ['here', 'are', 'some', 'words', 'with', 'punctuation']
            ],
            'special_signs' => [
                'Another (kind of a new) "test set"',
                ['another', 'kind', 'of', 'a', 'new', 'test', 'set']
            ],
            'exclude_numbers' => [
                'Microsoft Windows 10 build 1709',
                ['microsoft', 'windows', 'build']
            ],
            'exclude_repetitions' => [
                'New text for new test',
                ['new', 'text', 'for', 'test']
            ]
        ];
    }

    /**
     * @dataProvider wordsData
     *
     * @param string $text
     * @param array $expected
     */
    public function testGetWords(string $text, array $expected)
    {
        $item = $this->createMock(Item::class);
        $item->expects($this->once())
            ->method('getDescription')
            ->willReturn($text);

        $service = $this->createService();

        $result = $service->getWords($item);

        $this->assertSame($expected, $result);
    }

    protected function createService()
    {
        return new WordFrequencyCounterStub($this->cachePool);
    }
}
