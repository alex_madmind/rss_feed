<?php

namespace App\Tests\Unit\Service;

use App\Service\WordFrequencyCounter;
use App\Tests\Unit\TestCase;
use FeedIo\FeedInterface;
use FeedIo\FeedIo;
use FeedIo\Reader\Result;

class FeedReaderTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|FeedIo
     */
    protected $feed;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|WordFrequencyCounter
     */
    protected $counter;

    protected function setUp()
    {
        $this->feed = $this->createMock(FeedIo::class);
        $this->counter = $this->createMock(WordFrequencyCounter::class);
    }

    public function testGetFeed()
    {
        $feedMock = $this->createMock(FeedInterface::class);

        $readResult = $this->createMock(Result::class);
        $readResult->expects($this->once())
            ->method('getFeed')
            ->willReturn($feedMock);

        $this->feed->expects($this->once())
            ->method('read')
            ->with('http://localhost/feed.atom')
            ->willReturn($readResult);

        $service = $this->createService();

        $result = $service->getFeed();

        $this->assertSame($feedMock, $result);
    }

    public function testGetFeedCached()
    {
        $feedMock = $this->createMock(FeedInterface::class);

        $this->feed->expects($this->never())
            ->method('read');

        $service = $this->createService();
        $this->setObjectPropertyValue($service, 'feedCache', $feedMock);

        $result = $service->getFeed();

        $this->assertSame($feedMock, $result);
    }

    public function testFilterCommonWords()
    {
        $service = $this->createService();

        $result = $service->filterCommonWords([
            'lorem' => 10,
            'a' => 23,
            'ipsum' => 4,
            'b' => 13
        ]);

        $this->assertSame(['lorem' => 10, 'ipsum' => 4], $result);
    }

    public function testFilterCommonWordsReturnsEmptyList()
    {
        $service = $this->createService();

        $result = $service->filterCommonWords([
            'a' => 23,
            'b' => 13
        ]);

        $this->assertSame([], $result);
    }

    public function testGetFrequentWords()
    {
        $feed = $this->createMock(FeedInterface::class);
        $service = $this->createServiceMock(['getFeed', 'filterCommonWords']);
        $service->expects($this->once())
            ->method('getFeed')
            ->willReturn($feed);
        $service->expects($this->once())
            ->method('filterCommonWords')
            ->willReturn([
                'lorem' => 10,
                'dolor' => 23,
                'ipsum' => 4,
                'sit' => 13
            ]);

        $result = $service->getFrequentWords(2);

        $this->assertSame(['dolor' => 23, 'sit' => 13], $result);
    }

    protected function createService()
    {
        return new FeedReaderStub($this->feed, 'http://localhost/feed.atom', $this->counter, ['a', 'b', 'c']);
    }

    /**
     * @param array $methods
     * @return \PHPUnit\Framework\MockObject\MockObject|FeedReaderStub
     */
    protected function createServiceMock(array $methods)
    {
        return $this->getMockBuilder(FeedReaderStub::class)
            ->setConstructorArgs([$this->feed, 'http://localhost/feed.atom', $this->counter, ['a', 'b', 'c']])
            ->setMethods($methods)
            ->getMock();
    }
}
