<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase as BaseTestCase;
use ReflectionException;
use ReflectionProperty;

class TestCase extends BaseTestCase
{
    protected function getObjectPropertyValue($object, string $propertyName)
    {
        try {
            $property = new ReflectionProperty(get_class($object), $propertyName);
            $property->setAccessible(true);
            return $property->getValue($object);
        } catch (ReflectionException $e) {
            $this->fail('Failed getting property ' . $propertyName . ' of ' . get_class($object));
        }

        return null;
    }

    protected function setObjectPropertyValue($object, string $propertyName, $value): void
    {
        try {
            $property = new ReflectionProperty(get_class($object), $propertyName);
            $property->setAccessible(true);
            $property->setValue($object, $value);
        } catch (ReflectionException $e) {
            $this->fail('Failed getting property ' . $propertyName . ' of ' . get_class($object));
        }
    }
}
