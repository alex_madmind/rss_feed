# RSS Feed

## Requirements
 - PHP 7.1+ with SQLite
 - Composer

## Setup
 - Run `composer install`
 - Run `bin\console doctrine:migrations:migrate`

### Local server
Run `php -S 127.0.0.1:8000` from `public/` 
